var Person = require(process.cwd() + '/api/models/person');

module.exports = function(app, fs, bodyParser) {
    // serve client website using node webserver, will be Angular single-page application (SPA)
    app.get('/', function(request, response) {
        response.render('client_src/index');
    });
    // probably will want to have the API docs live in the client to be more Angular-ready
    app.get('/api', function(request, response) {
        response.render('api/views/api');
    });
    // GET /api/test will serve a sample json file
    app.route('/api/test')
        .get(function(request, response) {
            fs.readFile('api/json/test.json', function(err, data) {
                response.json(JSON.parse(data));
            });
        });

    app.route('/api/person')
        .post(function(req, res) {
            var person = new Person();
            for(var k in req.body) {
                if(Person.schema.paths[k]) person[k] = req.body[k];
            }
            if(!person.name) {
                res.json({ message: 'Data missing...', missingProperties: ['name'], dataSent: person });
                return;
            }
            person.save(function(err) {
                if(err) res.send(err);
                res.json({ message: 'Person added!', data: person });
            });
        })
        .get(function(req, res) {
            Person.find(function(err, persons) {
                if(err) res.send(err);
                res.json(persons);
            });
        });
    app.route('/api/person/:id')
        .get(function(req, res) {
            Person.findById(req.params.id, function(err, person) {
                if(err) res.send(err);
                res.json(person);
            });
        })
        .put(function(req, res) {
            Person.findById(req.params.id, function(err, person) {
                if(err) res.send(err);
                for(var k in req.body) {
                    if(Person.schema.paths[k]) person[k] = req.body[k];
                }
                person.increment();
                person.save(function(err) {
                    if(err) res.send(err);
                    res.json({ message: 'Person updated!', data: person });
                });
            });
        })
        .delete(function(req, res) {
            Person.findByIdAndRemove(req.params.id, function(err, person) {
                if(err) res.send(err);
                res.json({ message: 'Person deleted', data: person });
            });
        });
};