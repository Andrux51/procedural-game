## Robotic Memory Puzzle

Based on Sierra's *The Island of Dr. Brain* puzzle involving a programmable robot.

#### Player Goals
1. Send the robot to the correct location to collect a relic, then reach the drop location and drop off the relic.
    * The robot must be facing an adjacent relic to pick it up
    * The robot must be facing an adjacent drop location to drop off the relic
    * Colliding with any obstruction will send the robot back to its origin point and drop any relic being carried
2. Commands are issued through a control system designed specially for the robot
    * Pick Up and Drop Off commands can only be issued once per control session
    * The Drop Off command will execute the robot's command queue
    * Only one relic can be held at a time
    * Only the 3 most recent commands are visible while creating the command script
3. Collect all relics in the level to win!

#### Development Goals
1. ~~Create view area for puzzle game~~
2. ~~Use bootstrap glyphicons as placeholder graphics~~
3. Implement tile engine to make game map, including designating some tiles as impassable
    * ~~Any tile with a unit is, by nature, impassable~~
4. ~~Add player controls via clickable buttons - Forward, Backward, Turn Left, Turn Right, Pick Up, Drop Off, Execute, (Reset/Cancel?)~~
    * ~~Pick Up, Drop Off can only be used once per Execute~~
5. ~~Process actions for robot once Execute command is given~~
    * ~~Wait and watch as actions are processed~~
    * ~~Robot can only be given further commands once entire process is finished (possibly add quick-cancel button for convenience)~~
6. Implement game rules
    * If robot collides with anything, retrace the steps made to bring it back to the origin point
    * ~~Only up to 3? previous commands are shown at a time, so the player will have to map their steps in their head~~

##### Optional Feature Stretch Goals
1. Add css spritesheet for game graphics
    * Include some animation frames for robot
2. Set up algorithm to procedurally generate new levels on demand
    * Maps must be valid (have a proper solution)
    * Maps must contain at least 2 relics, which must be several tiles away from each other
2. In-game clock
    * Show real time for player to keep track of the clock in the real world
    * Show amount of time elapsed in game
        * This can created strategic factors if too much time spent on a single mission
3. Allow login with OAuth
    * Let player save their progress across multiple sessions in database

##### Difficulty Adjustment Stretch Goals
1. "Bugs" in programming - Opposite action will occur (Turn Left = Turn Right, Pick Up = Drop Off, etc)
    * For a harder game, have weirder bugs, e.g. 90 degree right adjustment to movement (Turn Left = Forward)
    * For hardest game, have uncontrolled bugs that force the player to rely on luck to succeed
2. Do not show player what they've entered into the program
    * For an easier game, show player all commands entered into the program
    * For easiest game, show a "ghost robot" that will enact the commands given (remove memory component from difficulty)
3. Enemies - stationary
    * Implement animation frames for enemies
    * Moving enemies (To make this fair, enemy movement can be shown during the programming phase)
4. Timed missions
    * Harder - Have a limited amount of time to perform a mission
    * Time finished saved to player stats, leaderboard in db

##### Social Stretch Goals
1. Share this puzzle
    * Each tilemap will be input into a DB and have a serial identifier
    * Provide link to `{gameurl}/{puzzle_id}` and allow interaction with various social media outlets
2. Puzzle editor
    * Allow players to make their own map
    * Implement checks to ensure that a created map is possible to be completed
3. Speedrunner mode
    * Allow player to skip all robot animations
        * Robot will simply move without animating, relic will disappear when picked up, etc.
        * Still have some graphical accompinament to play, but allow game to be completed as rapidly as possible
